<?php

switch($_SERVER["REQUEST_METHOD"]) {
    //obtener Datos
    case "GET":
    if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) &&
            strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
                $funcion = $_GET['funcion'];
                $i_ctrl = new Inmueble_controller();
                switch($funcion){
                    case "mostrarTodos":
                        $i_ctrl->mostrarTodos();
                    break;
                    case "llenarFiltrosListas":
                        $i_ctrl->consultarbyFiltro();
                    break;
                    case "ConsultaFiltro":
                        $rango_precio = $_GET['precio'];
                        $precios = explode(";", $rango_precio);
                        $ciudad = $_GET['selectCiudad'];
                        $tipo = $_GET['selectTipo'];
                        $i_ctrl->ConsultaFiltro($precios[0], $precios[1], $ciudad, $tipo);
                    break;
                }
    }
}

class Inmueble_controller{

    public function mostrarTodos(){
        $url = "../data-1.json";
        $file = file_get_contents($url); 
        $data = json_decode($file); 
        if (count($data) > 0) {
            echo json_encode(array('error' => false, 'data' => $data));
        } else {
            $data = '<p>No hay datos para mostrar</p>';
            echo json_encode(array('error' => true, 'data' => $flag));
        }
    }

    public function consultarbyFiltro(){
        $url = "../data-1.json";
        $file = file_get_contents($url); 
        $datos = json_decode($file);
        $data1 = array();
        $data2 = array();
        $pos = 0;
        foreach($datos as $value){
            $data1[$pos] = $value->Ciudad;
            $data2[$pos] = $value->Tipo;
            $pos++;
         }
        $data = ["ciudad"=>array_unique($data1), "tipo"=>array_unique($data2)];
        if (count($data) > 0) {
            echo json_encode(array('error' => false, 'data' => $data));
        } else {
            echo json_encode(array('error' => true, 'data' => false));
        }
    }

    public function ConsultaFiltro($precio1, $precio2, $ciudad, $tipo){
        $url = "../data-1.json";
        $file = file_get_contents($url); 
        $datos = json_decode($file); 
        $data = array();
        $pos = 0;
        $caracteres_spr = array('$',',');
        $newPrecio;
        $comparacionPrecios;
        if($ciudad != '' && $tipo == ''){
            //print_r("Ciudad Digitada | Tipo No Digitado");
            foreach($datos as $value){
                $newPrecio = doubleval(str_replace($caracteres_spr,"",$value->Precio));
                $comparacionPrecios = ($newPrecio >= $precio1 && $newPrecio <= $precio2);
                if($value->Ciudad == $ciudad && $comparacionPrecios){
                    $data[$pos] = $value;
                    $pos++;
                }
            }
        }else{
            if($ciudad == '' && $tipo != ''){
                //print_r("Ciudad No Digitada | Tipo Digitado");
                foreach($datos as $value){
                    $newPrecio = doubleval(str_replace($caracteres_spr,"",$value->Precio));
                    $comparacionPrecios = ($newPrecio >= $precio1 && $newPrecio <= $precio2);
                    if($value->Tipo == $tipo && $comparacionPrecios){
                        $data[$pos] = $value;
                        $pos++;
                    }
                }
            }else{
                if($ciudad != '' && $tipo != ''){
                    //print_r("Ciudad Digitada | Tipo Digitado");
                    foreach($datos as $value){
                        $newPrecio = doubleval(str_replace($caracteres_spr,"",$value->Precio));
                        $comparacionPrecios = ($newPrecio >= $precio1 && $newPrecio <= $precio2);
                        if($value->Tipo == $tipo && $value->Ciudad == $ciudad && $comparacionPrecios){
                            $data[$pos] = $value;
                            $pos++;
                        }
                    }
                }else{
                    if($ciudad == '' && $tipo == ''){
                        //print_r("Ciudad No Digitada | Tipo No Digitado");
                        foreach($datos as $value){
                            $newPrecio = doubleval(str_replace($caracteres_spr,"",$value->Precio));
                            $comparacionPrecios = ($newPrecio >= $precio1 && $newPrecio <= $precio2);
                            if($comparacionPrecios){
                                $data[$pos] = $value;
                                $pos++;
                            }
                        }
                    }                   
                }
            }
        }
        if (count($data) > 0) {
            echo json_encode(array('error' => false, 'data' => $data));
        } else {
            echo json_encode(array('error' => true, 'data' => false));
        }
    }


}
?>