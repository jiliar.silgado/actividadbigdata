/*
  Creación de una función personalizada para jQuery que detecta cuando se detiene el scroll en la página
*/
$.fn.scrollEnd = function(callback, timeout) {
    $(this).scroll(function() {
        var $this = $(this);
        if ($this.data('scrollTimeout')) {
            clearTimeout($this.data('scrollTimeout'));
        }
        $this.data('scrollTimeout', setTimeout(callback, timeout));
    });
};
/*
  Función que inicializa el elemento Slider
*/

function inicializarSlider() {
    $("#rangoPrecio").ionRangeSlider({
        type: "double",
        grid: false,
        min: 0,
        max: 100000,
        from: 200,
        to: 80000,
        prefix: "$"
    });
}
/*
  Función que reproduce el video de fondo al hacer scroll, y deteiene la reproducción al detener el scroll
*/
function playVideoOnScroll() {
    var ultimoScroll = 0,
        intervalRewind;
    var video = document.getElementById('vidFondo');
    $(window)
        .scroll((event) => {
            var scrollActual = $(window).scrollTop();
            if (scrollActual > ultimoScroll) {
                video.play();
            } else {
                //this.rewind(1.0, video, intervalRewind);
                video.play();
            }
            ultimoScroll = scrollActual;
        })
        .scrollEnd(() => {
            video.pause();
        }, 10)
}

inicializarSlider();
playVideoOnScroll();

$('#mostrarTodos').click(function() {
    mostrarTodos();
});

function mostrarTodos() {

    var request = $.ajax({
        url: 'controllers/Inmueble_controller.php',
        type: 'GET',
        data: { funcion: 'mostrarTodos' },
        dataType: 'json'
    });

    request.done(function(respuesta) {
        var json = JSON.stringify(respuesta['data']);
        var data = JSON.parse(json);
        var contenido = '';
        $('.contenido-tarjetas').html('');
        if (data.length > 0) {
            for (var i = 0; i < data.length; i++) {
                contenido += Tarjeta().replace(':id:', data[i].Id)
                    .replace(':direccion:', data[i].Direccion)
                    .replace(':ciudad:', data[i].Ciudad)
                    .replace(':telefono:', data[i].Telefono)
                    .replace(':codigo_postal:', data[i].Codigo_Postal)
                    .replace(':tipo:', data[i].Tipo)
                    .replace(':precio:', data[i].Precio);
            }
        } else {
            contenido = '<div id="titulo-info">No hay datos para mostrar</div>';
        }
        $('.contenido-tarjetas').html(contenido);
    });

    request.fail(function(resp) {
        console.log("Error");
    })

    request.always(function() {
        console.log("complete");
    });
}

function Tarjeta() {
    var tarjeta = "<div class='contenido-card card clearfix'>" +
        "<div id='linea'>" +
        "<div class='col l4 m4 s4 left-align' id='img-contenido-card'>" +
        "<img src='img/home.jpg' class='responsive-img '/>" +
        "</div>" +
        "<div class='col l8 m8 s8 left-align' id='info-contenido-card'>" +
        /*"<div id='linea'>" +
        "<div id='titulo-info'>Id: </div>" +
        "<div id='valor-info'>:id:</div>" +
        "</div>" +*/
        "<div id='linea'>" +
        "<div id='titulo-info' >Dirección: </div>" +
        "<div id='valor-info' >:direccion:</div>" +
        "</div>" +
        "<div id='linea'>" +
        "<div id='titulo-info'>Ciudad: </div>" +
        "<div id='valor-info'>:ciudad:</div>" +
        "</div>" +
        "<div id='linea'>" +
        "<div id='titulo-info'>Telefono: </div>" +
        "<div id='valor-info'>:telefono:</div>" +
        "</div>" +
        "<div id='linea'>" +
        "<div id='titulo-info'>Codigo Postal: </div>" +
        "<div id='valor-info' >:codigo_postal:</div>" +
        "</div>" +
        "<div id='linea'>" +
        "<div id='titulo-info' >Tipo: </div>" +
        "<div id='valor-info'>:tipo:</div>" +
        "</div>" +
        "<div id='linea'>" +
        "<div id='titulo-info'>Precio:</div>" +
        "<div id='valor-info' class='precioTexto'>:precio:</div>" +
        "</div>" +
        "<div class='divider' style='margin-top: 40px;'></div>" +
        "<div id='linea'>" +
        "<a class='waves-effect waves-light btn-small' id='ver-mas'>VER MAS</a>" +
        "</div>" +
        "</div>" +
        "</div>" +
        "</div>";
    return tarjeta;
}

//Llenado de Filtros Tipo Select


function llenarFiltrosListas() {

    var request = $.ajax({
        url: 'controllers/Inmueble_controller.php',
        type: 'GET',
        data: { funcion: 'llenarFiltrosListas' },
        dataType: 'json'
    });

    request.done(function(respuesta) {
        var json = JSON.stringify(respuesta['data']);
        var data = JSON.parse(json);
        $.each(data.ciudad, function(index, value) {
            option = "<option value='" + value + "'>" + value + "</option>";
            $('#selectCiudad').append(option);
        });
        $.each(data.tipo, function(index, value) {
            option = "<option value='" + value + "'>" + value + "</option>";
            $('#selectTipo').append(option);
        });
    });

    request.fail(function(resp) {
        console.log("Error");
    })

    request.always(function() {
        console.log("complete");
    });
}


jQuery(document).on('submit', '#formulario', function(event) {
    event.preventDefault();
    jQuery.ajax({
        url: 'controllers/Inmueble_controller.php',
        type: 'GET',
        dataType: 'json',
        data: $(this).serialize()
    })

    .done(function(respuesta) {
        var json = JSON.stringify(respuesta['data']);
        var data = JSON.parse(json);
        var contenido = '';
        $('.contenido-tarjetas').html('');
        if (data.length > 0) {
            for (var i = 0; i < data.length; i++) {
                contenido += Tarjeta().replace(':id:', data[i].Id)
                    .replace(':direccion:', data[i].Direccion)
                    .replace(':ciudad:', data[i].Ciudad)
                    .replace(':telefono:', data[i].Telefono)
                    .replace(':codigo_postal:', data[i].Codigo_Postal)
                    .replace(':tipo:', data[i].Tipo)
                    .replace(':precio:', data[i].Precio);
            }
        } else {
            contenido = '<div id="titulo-info">No hay datos para mostrar</div>';
        }
        $('.contenido-tarjetas').html(contenido);
    })

    .fail(function(resp) {
        console.log("Error");
    })

    .always(function() {
        console.log("complete");
    });
});